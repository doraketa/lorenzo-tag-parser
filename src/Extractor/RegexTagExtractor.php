<?php

declare(strict_types=1);

namespace TagParser\Extractor;

class RegexTagExtractor implements TagExtractorInterface
{
    /**
     * @param  string $html
     * @return array
     */
    public function extractTags(string $html): array
    {
        $pattern = '/<\s*([a-zA-Z0-9]+)(?:\s|>)/';
        preg_match_all($pattern, $html, $matches);

        return $matches[1];
    }
}
