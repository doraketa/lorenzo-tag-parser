<?php

declare(strict_types=1);

namespace TagParser\Extractor;

interface TagExtractorInterface
{
    /**
     * Метод для извлечения тэгов по регулярному (regex) выражению
     *
     * @param  string $html
     * @return array
     */
    public function extractTags(string $html): array;
}
