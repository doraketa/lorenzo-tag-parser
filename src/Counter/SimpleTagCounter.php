<?php

declare(strict_types=1);

namespace TagParser\Counter;

class SimpleTagCounter implements TagCounterInterface
{
    /**
     * @param  array $tags
     * @return array
     */
    public function countTagOccurrences(array $tags): array
    {
        return array_count_values(array_map('strtolower', $tags));
    }
}
