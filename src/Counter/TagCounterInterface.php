<?php

declare(strict_types=1);

namespace TagParser\Counter;

interface TagCounterInterface
{
    /**
     * Метод для посчета тэгов
     *
     * @param  array $tags
     * @return array
     */
    public function countTagOccurrences(array $tags): array;
}
