<?php

declare(strict_types=1);

namespace TagParser\HTMLParser;

use TagParser\Counter\TagCounterInterface;
use TagParser\Extractor\TagExtractorInterface;

class HTMLParserFactory
{
    private TagExtractorInterface $tagExtractor;
    private TagCounterInterface $tagCounter;

    public function __construct(TagExtractorInterface $tagExtractor, TagCounterInterface $tagCounter)
    {
        $this->tagExtractor = $tagExtractor;
        $this->tagCounter = $tagCounter;
    }

    /**
     * Фабричный метод для создания экземпляра парсера
     *
     * @param  string              $html
     * @return HTMLParserInterface
     */
    public function createHTMLParser(string $html): HTMLParserInterface
    {
        return new HTMLParser($html, $this->tagExtractor, $this->tagCounter);
    }
}
