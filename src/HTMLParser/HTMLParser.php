<?php

declare(strict_types=1);

namespace TagParser\HTMLParser;

use TagParser\Counter\TagCounterInterface;
use TagParser\Extractor\TagExtractorInterface;
use TagParser\HtmlParser\HTMLParserInterface;

class HTMLParser implements HTMLParserInterface
{
    private string $html;
    private TagExtractorInterface $tagExtractor;
    private TagCounterInterface $tagCounter;

    public function __construct(string $html, TagExtractorInterface $tagExtractor, TagCounterInterface $tagCounter)
    {
        $this->html = $html;
        $this->tagExtractor = $tagExtractor;
        $this->tagCounter = $tagCounter;
    }

    /**
     * Подсчет тэгов
     *
     * @param  string $html
     * @return array
     */
    public function countTags(string $html): array
    {
        $tags = $this->tagExtractor->extractTags($html);
        return $this->tagCounter->countTagOccurrences($tags);
    }
}
