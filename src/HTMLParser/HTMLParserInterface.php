<?php

declare(strict_types=1);

namespace TagParser\HTMLParser;

interface HTMLParserInterface
{
    /**
     * Метод для подсчет тэгов
     *
     * @param  string $html
     * @return array
     */
    public function countTags(string $html): array;
}
