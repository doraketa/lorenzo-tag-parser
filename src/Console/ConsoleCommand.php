<?php

declare(strict_types=1);

namespace TagParser\Console;

class ConsoleCommand extends AbstractCommand
{
    protected function processHTML(string $html): void
    {
        $tagCounts = $this->htmlParser->countTags($html);

        foreach ($tagCounts as $tag => $count) {
            echo "$tag: $count\n";
        }
    }
}
