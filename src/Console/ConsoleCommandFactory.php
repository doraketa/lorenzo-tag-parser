<?php

declare(strict_types=1);

namespace TagParser\Console;

use TagParser\HTMLParser\HTMLParserFactory;

class ConsoleCommandFactory
{
    private HTMLParserFactory $htmlParserFactory;

    public function __construct(HTMLParserFactory $htmlParserFactory)
    {
        $this->htmlParserFactory = $htmlParserFactory;
    }

    /**
     * Метод для инициализации команды
     *
     * @return CommandInterface
     */
    public function createCommand(): CommandInterface
    {
        $htmlParser = $this->htmlParserFactory->createHTMLParser('');
        return new ConsoleCommand($htmlParser);
    }
}
