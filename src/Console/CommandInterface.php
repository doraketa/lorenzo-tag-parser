<?php

declare(strict_types=1);

namespace TagParser\Console;

interface CommandInterface
{
    /**
     * Метод исполнения команды
     *
     * @param  array $arguments
     * @return void
     */
    public function execute(array $arguments): void;
}
