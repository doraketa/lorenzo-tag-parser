<?php

declare(strict_types=1);

namespace TagParser\Console;

use TagParser\HTMLParser\HTMLParserInterface;

abstract class AbstractCommand implements CommandInterface
{
    protected HTMLParserInterface $htmlParser;

    public function __construct(HTMLParserInterface $htmlParser)
    {
        $this->htmlParser = $htmlParser;
    }

    /**
     * @param  array $arguments
     * @return void
     */
    public function execute(array $arguments): void
    {
        $url = $arguments[0] ?? '';

        if ($url === '') {
            echo "Please provide a URL.\n";
            return;
        }

        $html = file_get_contents($url);
        $this->processHTML($html);
    }

    /**
     * Абстрактный метод для наследования, для возможности гибкой настройки output-информации
     *
     * @param  string $html
     * @return void
     */
    abstract protected function processHTML(string $html): void;
}
