# HTML TagParser

## Описание проекта

Данный проект реализован в качестве тестового, и не несет за собой никакой смысловой нагрузки, кроме демонстрации кода.

## Примененные паттерны и принципы

**Фабричный метод (Factory Method):** Использование фабричного метода в классе CommandFactory для создания экземпляра команды HTMLTagsCountCommand. Фабричный метод инкапсулирует процесс создания объекта и позволяет легко добавлять новые типы команд в систему.

**Интерфейсы (Interfaces):** Использование интерфейсов для определения контрактов классов. Например, интерфейс HTMLParserInterface определяет метод countTags(), которому должны соответствовать классы, реализующие этот интерфейс.

**Одиночка (Singleton):** Применение паттерна одиночка в классе CommandFactory, где через статическое свойство instance и статический метод getInstance() гарантируется существование только одного экземпляра класса.

**недрение зависимостей (Dependency Injection):** Внедрение зависимостей через конструкторы классов. Например, класс HTMLParser принимает в конструкторе экземпляры TagExtractorInterface и TagCounterInterface, что позволяет легко заменять или расширять поведение этих компонентов.

**Принцип единственной ответственности (Single Responsibility Principle):** Каждый класс отвечает только за одну четко определенную обязанность. Например, TagExtractor отвечает за извлечение тегов из HTML, TagCounter — за подсчет количества тегов, HTMLParser — за объединение этих компонентов и выполнение общей задачи.

**Принцип открытости/закрытости (Open/Closed Principle):** Код разработан с учетом возможности расширения функциональности без изменения существующего кода. Например, можно добавить новые типы команд, реализуя интерфейс CommandInterface и создавая новые классы команд, не изменяя код класса CommandFactory.

**Принцип инверсии зависимостей (Dependency Inversion Principle):** Зависимости программы строятся на абстракциях, а не на конкретных реализациях. Например, класс ConsoleCommand зависит от абстракций HTMLParserInterface, TagExtractorInterface и TagCounterInterface, что позволяет подставлять различные реализации этих интерфейсов без изменения кода ConsoleCommand.

## Authors

- [@doraketa](https://gitlab.com/doraketa)

## License

[MIT](https://choosealicense.com/licenses/mit/)